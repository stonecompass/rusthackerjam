extern crate piston_window;
extern crate find_folder;

use piston_window::*;

fn main() {
    let opengl = OpenGL::V3_2;
    let mut window: PistonWindow = WindowSettings::new("Game test", [400, 400])
    .opengl(opengl)
    .exit_on_esc(true)
    .build()
    .unwrap();

    let assets = find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("assets").unwrap();
    let rust_logo = Texture::from_path(&mut window.factory,
                                       assets.join("rust.png"),
                                       Flip::None,
                                       &TextureSettings::new()).unwrap();

    while let Some(e) = window.next() {
        window.draw_2d(&e, |c, g| {
            clear([1.0, 1.0, 1.0, 1.0], g);
            Rectangle::new([0.5, 0.2, 0.2, 1.0])
          		.draw([200.0, 200.0, 100.0, 100.0], &c.draw_state, c.transform, g);
            let transform = c.transform.trans(100.0, 100.0);
            Image::new().draw(&rust_logo,
                 &DrawState::new_outside(),
                transform, g);
        });
    }
}